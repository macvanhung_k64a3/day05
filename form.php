<?php
ob_start();
session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet prefetch" href="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css">
    <script src="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

    <title>Đăng kí sinh viên</title>

    <link rel="stylesheet" href="confirm.css">

</head>

<body>
    <div id="main">
        <div class="wrapper">
            <div class="showError">
                <?php
                $nameErr = $khoaErr = $sexErr = $dobErr = $addressErr = "";
                $name = $khoa = $sex = $dob = $address = $image = "";


                if (isset($_POST['submit'])) {

                    if (empty($_POST["name"])) {
                        $nameErr = "Hãy nhập họ tên";
                        echo '<p style="color:red;">' . $nameErr . '</p>';
                    } 
                    if (empty($_POST["khoa"]) || $_POST["khoa"] == 'None') {
                        $khoaErr = "Hãy nhập  tên khoa";
                        echo '<p style="color:red;">' . $khoaErr . '</p>';
                    } else {
                        $khoa = check_ip($_POST['khoa']);
                    }


                    if (empty($_POST["sex"])) {
                        $sexErr = "Bạn cần nhập giới tính";
                        echo '<p style="color:red;">' . $sexErr . '</p>';
                    } else {
                        $sex = check_ip($_POST['sex']);
                    }

                    if (empty($_POST["dob"])) {
                        $dobErr = "Hãy nhập ngày sinh";
                        echo '<p style="color:red;">' . $dobErr . '</p>';
                    } else {
                        $dob = check_ip($_POST['dob']);
                    }
                    $address = check_ip($_POST['address']);

                    if ($_FILES["img"]["error"] == 0) {
                        $fileExt = explode('.', $_FILES['img']['name']);
                        $fileFormat = strtolower(end($fileExt));
                        $allowed = array('jpg', 'jpeg', 'png', 'pdf');
                        if (!in_array($fileFormat, $allowed)) {
                            $errors = $errors . '<div class="errors">Hãy chọn file có định dạng jpg, jpeg, png hoặc pdf.<br></div>';
                        }
                        else {
                            if (!file_exists('upload')) {
                                mkdir('upload', 0777, true);
                            }
            
                            date_default_timezone_set('Asia/Ho_Chi_Minh');
                            $date_time = date('YmdHis', time());
            
                            $fileNameNew = implode(".", array_slice($fileExt, 0, -1)) . '_' . $date_time . '.' . $fileFormat;
                            $path = 'upload/' . $fileNameNew;
                            move_uploaded_file($_FILES['img']['tmp_name'], $path);
                            $_POST['img'] = $path;
                        }
                    }
            
                    if (empty($addressErr) && empty($dobErr) && empty($sexErr) && empty($khoaErr) && empty($dobErr) && empty($nameErr)) {
                        $_SESSION = $_POST;


                        header("Location: confirm_Form.php");
                    }
                }

                function check_ip($data)
                {
                    $data = trim($data);
                    $data = stripslashes($data);
                    $data = htmlspecialchars($data);
                    return $data;
                }

                ?>
            </div>
            <form method="post" enctype="multipart/form-data">
                <div class="form-group">
                    <label class="form-label">Họ và tên <span class="required">*</span></label>
                    <div class="input-use">
                        <input type="text" name="name" id="name" value="<?php echo $name ?>">
                    </div>

                </div>
                <div class="form-group">
                    <label class="form-label">Giới tính <span class="required">*</span></label>
                    <div class="input-use">
                        <?php
                        $sexArr = array("0" => "Nam", "1" => "Nữ");
                        for ($i = 0; $i < count($sexArr); $i++) {
                            echo '  <div class="radio-gr">
                                        <input type="radio"  name="sex" value=" ' . $i . '"  >
                                        <label class="radio-label" for="' . $sexArr[$i] . '">' . $sexArr[$i] . '</label>
                                    </div>';
                        }
                        ?>
                    </div>


                </div>
                <div class="form-group">
                    <label class="form-label">Phân khoa <span class="required">*</span></label>
                    <div class="input-use">
                        <select name="khoa" id="phankhoa">
                            <?php
                            $khoa = array("None" => "", "MAT" => "Khoa học máy tính", "KDL" => "Khoa học vật liệu");
                            foreach ($khoa as $key => $value) {
                                echo '<option value="' . $key . '">' . $value . '</option>';
                            }
                            ?>
                        </select>
                    </div>


                </div>
                <div class="form-group">
                    <label class="form-label">Ngày sinh <span class="required">*</span></label>
                    <div class="input-use">
                        <input id="dp" type="text" name="dob" placeholder="dd/mm/yyyy" value="<?php echo $dob ?>" />

                    </div>

                </div>
                <div class="form-group">
                    <label class="form-label">Địa chỉ</label>
                    <div class="input-use">
                        <input type="text" name="address" id="address" value="<?php echo $address ?>">
                    </div>

                </div>
                <div class="form-group">
                    <label class="form-label">Hình ảnh</label>
                    <div class="input-use">
                        <input type="file" name="img" id="image" style="margin-right:-12px;" onchange="readURL(this);">

                    </div>

                </div>
                <input class="img_encode" name="img_encode" style="display: none;" />
                
                <div class="button-box">
                    <input type="submit" name="submit" class="sub_btn" value="Đăng kí">
                </div>
                
            </form>
        </div>
    </div>
    <Script>
        function readURL(input) {
            console.log(input.files[0]);
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                $('#blah').css("display", "block");
                reader.onload = function(e) {
                    $('#blah')
                        .attr('src', e.target.result);

                    $('.img_encode').val(e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            } else {
                $('#blah').css("display", "none");
            }
        }
    </Script>
    <script type="text/javascript" src='https://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.3.min.js'></script>
    <script type="text/javascript" src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/js/bootstrap.min.js'></script>
    <link rel="stylesheet" href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/css/bootstrap.min.css' media="screen" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" type="text/css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function() {
            $('#dp').datetimepicker({
                format: "dd/mm/yyyy"
            });
        });
    </script>

</body>

</html>
