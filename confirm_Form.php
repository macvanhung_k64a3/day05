<?php
session_start();
$img = $_SESSION["img_encode"]
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet prefetch" href="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css">
    <script src="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <title>Đăng kí sinh viên</title>
    
    <link rel="stylesheet" href="confirm.css">
</head>

<body>
    <div id="main">
        <div class="wrapper">
            <div class="showError">
            </div>
            <div class="formDisplay">
                <div class="form-group">
                    <label class="form-label">Họ và tên <span class="required">*</span></label>
                    <div class="input-use">
                        <?php echo '<p id="name">' . $_SESSION["name"] . '</p>' ?>
                    </div>

                </div>
                <div class="form-group">
                    <label class="form-label">Giới tính <span class="required">*</span></label>
                    <div class="input-use">
                        <?php
                        $sex = "";
                        if ($_SESSION["sex"] == '0') {
                            $sex = "Nam";
                        } else {
                            $sex = "Nữ";
                        }

                        echo '<p id="sex">' . $sex . '</p>' ?>
                    </div>


                </div>
                <div class="form-group">
                    <label class="form-label">Phân khoa <span class="required">*</span></label>
                    <div class="input-use">
                        <?php
                        $khoa = "";
                        if ($_SESSION["khoa"] == 'MAT') {
                            $khoa = "Khoa học máy tính";
                        } else if ($_SESSION["khoa"] == 'KDL') {
                            $khoa = "Khoa học vật liệu";
                        }
                        echo '<p id="khoa">' . $khoa . '</p>' ?>
                    </div>


                </div>
                <div class="form-group">
                    <label class="form-label">Ngày sinh <span class="required">*</span></label>
                    <div class="input-use">
                        <?php echo '<p id="dob">' . $_SESSION["dob"] . '</p>' ?>

                    </div>

                </div>
                <div class="form-group">
                    <label class="form-label">Địa chỉ</label>
                    <div class="input-use">
                        <?php
                        echo '<p id="address">' . $_SESSION["address"] . '</p>'
                        ?>
                    </div>

                </div>
                <div class="form-group">
                    <label class="form-label">Hình ảnh</label>
                    <div class="input-use">
                        <?php
                        if ($img) {
                            echo '<img class="studentImg" src="' . $_SESSION["img"] . '" alt="Student avatar">';
                        } ?>
                    </div>

                </div>
                <input type="submit" name="submit" class="sub_btn" value="Xác nhận">

            </div>
        </div>
    </div>
    <script type="text/javascript" src='https://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.3.min.js'></script>
    <script type="text/javascript" src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/js/bootstrap.min.js'></script>
    <link rel="stylesheet" href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/css/bootstrap.min.css' media="screen" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" type="text/css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function() {
            $('#dp').datetimepicker({
                format: "dd/mm/yyyy"
            });
        });
    </script> -->

</body>

</html>
